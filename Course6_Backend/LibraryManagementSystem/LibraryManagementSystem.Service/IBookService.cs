﻿using LibraryManagementSystem.Model;
using System.Collections.Generic;

namespace LibraryManagementSystem.Service
{
    public interface IBookService
    {
        void DeleteBookById(string bookId);
        List<Book> GetBookDataByCondition(BookSearchArg arg);
        Book GetBookDetailById(int bookId);
        List<BookSearchArg> GetBookIdNameData();
        List<BookLendRecord> GetBookLendRecord(int bookId);
        int InsertBook(Book book);
        void UpdateBookData(Book updateBookData, Book originBookData);
        bool IsDeletable(int bookId);
    }
}