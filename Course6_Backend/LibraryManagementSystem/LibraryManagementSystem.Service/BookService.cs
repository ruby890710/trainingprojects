﻿using LibraryManagementSystem.Dao;
using System.Collections.Generic;

namespace LibraryManagementSystem.Service
{
    public class BookService : IBookService
    {
        private BookDao bookDao { get; set; }
        /// <summary>
        /// 取得書名資料
        /// </summary>
        /// <returns></returns>
        public List<LibraryManagementSystem.Model.BookSearchArg> GetBookIdNameData()
        {
            return bookDao.GetBookIdNameData();
        }

        /// <summary>
        /// 依照條件取得圖書資料
        /// </summary>
        /// <returns></returns>
        /// Ioc/DI
        public List<LibraryManagementSystem.Model.Book> GetBookDataByCondition(LibraryManagementSystem.Model.BookSearchArg arg)
        {
            return bookDao.GetBookDataByCondition(arg);
        }

        /// <summary>
        /// 取得圖書明細資料
        /// </summary>
        /// <returns></returns>
        public LibraryManagementSystem.Model.Book GetBookDetailById(int bookId)
        {
            return bookDao.GetBookDetailById(bookId);
        }

        /// <summary>
        /// 新增圖書資料
        /// </summary>
        /// <param name="book"></param>
        /// <returns></returns>
        public int InsertBook(LibraryManagementSystem.Model.Book book)
        {
            return bookDao.InsertBook(book);
        }

        /// <summary>
        /// 編輯圖書資料(修改)
        /// </summary>
        /// <returns></returns>
        public void UpdateBookData(LibraryManagementSystem.Model.Book updateBookData, LibraryManagementSystem.Model.Book originBookData)
        {
            bookDao.UpdateBookData(updateBookData, originBookData);
        }

        /// <summary>
        /// 刪除圖書資料
        /// </summary>
        public void DeleteBookById(string bookId)
        {
            bookDao.DeleteBookById(bookId);
        }

        /// <summary>
        /// 取得圖書借閱紀錄
        /// </summary>
        /// <returns></returns>
        public List<LibraryManagementSystem.Model.BookLendRecord> GetBookLendRecord(int bookId)
        {
            return bookDao.GetBookLendRecord(bookId);
        }

        public bool IsDeletable(int bookId)
        {
            BookDao bookDao = new BookDao();
            LibraryManagementSystem.Model.Book result = bookDao.GetBookDetailById(bookId);
            // 若借閱狀態為B(已借出)或C(已借出未領)，不可刪除書籍
            if (result.BookStatusId == "B" || result.BookStatusId == "C")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

}
