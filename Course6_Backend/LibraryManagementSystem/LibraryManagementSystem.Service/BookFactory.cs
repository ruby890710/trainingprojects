﻿using LibraryManagementSystem.Dao;

namespace LibraryManagementSystem.Service
{
    public class BookFactory
    {
        public IBookDao GetBookDao()
        {
            IBookDao result;

            switch (Common.ConfigTool.GetAppsetting("DaoInTest"))
            {
                case "Y":
                    result = new LibraryManagementSystem.Dao.BookTestDao();
                    break;
                case "N":
                    result = new LibraryManagementSystem.Dao.BookDao();
                    break;
                default:
                    result = new LibraryManagementSystem.Dao.BookTestDao();
                    break;
            }
            return result;
        }
    }
}
