﻿using System.Collections.Generic;
using System.Data;

namespace LibraryManagementSystem.Common
{
    public interface IBookDataAccessTool
    {
        DataTable Query(string connectionString, string sql, List<KeyValuePair<string, object>> parameters);
    }
}