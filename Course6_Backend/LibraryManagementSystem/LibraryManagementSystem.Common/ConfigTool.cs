﻿namespace LibraryManagementSystem.Common
{
    public class ConfigTool
    {
        /// <summary>
        /// 取得DB連線字串
        /// </summary>
        /// <returns></returns>
        // 工具程式設public static
        public static string GetDBConnectionString(string connName)
        {
            return
                System.Configuration.ConfigurationManager.
                ConnectionStrings[connName].ConnectionString.ToString();
        }
        public static string GetAppsetting(string Key)
        {
            string AppSetting = string.Empty;
            AppSetting = System.Configuration.ConfigurationManager.AppSettings[Key];
            return AppSetting;
        }
    }
}
