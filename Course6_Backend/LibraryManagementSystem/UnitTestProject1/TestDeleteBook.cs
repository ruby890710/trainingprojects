﻿using LibraryManagementSystem.Common;
using LibraryManagementSystem.Dao;
using LibraryManagementSystem.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Data;

namespace UnitTestProject1
{
    [TestClass]
    public class TestDeleteBook
    {
        [TestMethod]
        public void TestBookIsDelectable()
        {
            // Arrange
            IList<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            parameters.Add(new KeyValuePair<string, object>("@BookId", 1522));  //1522：資訊夢工廠#1
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("BookId");
            dataTable.Columns.Add("BookStatusId");

            dataTable.Rows.Add(1522, "B");
            var mock = new Mock<IBookDataAccessTool>();
            mock.Setup(m => m.Query(It.IsAny<string>(), It.IsAny<string>(), (List<KeyValuePair<string, object>>)parameters)).Returns(dataTable);
            BookDao bookDao = new BookDao();
            bookDao.BookDataAccessTool = mock.Object;

            //Act
            BookService bookService = new BookService();
            bool isDeletable = bookService.IsDeletable(1522);

            //Assert
            Assert.IsFalse(isDeletable);
        }
    }
}
