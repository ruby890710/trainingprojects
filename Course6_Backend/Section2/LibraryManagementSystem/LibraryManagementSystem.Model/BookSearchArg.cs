﻿using System.ComponentModel;

namespace LibraryManagementSystem.Model
{
    public class BookSearchArg
    {
        [DisplayName("書號")]
        public int BookId { get; set; }
        [DisplayName("書名")]
        public string BookName { get; set; }
        [DisplayName("圖書類別")]
        public string BookClassName { get; set; }
        [DisplayName("圖書類別代號")]
        public string BookClassId { get; set; }
        [DisplayName("借閱人")]
        public string BookKeeperName { get; set; }
        [DisplayName("借閱人代號")]
        public string BookKeeperId { get; set; }

        [DisplayName("借閱狀態")]
        public string StatusName { get; set; }
        [DisplayName("借閱狀態代號")]
        public string BookStatusId { get; set; }
    }

}
