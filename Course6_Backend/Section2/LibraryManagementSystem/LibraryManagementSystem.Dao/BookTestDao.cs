﻿using LibraryManagementSystem.Model;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace LibraryManagementSystem.Dao
{
    // IoC/DI -降低耦合度
    // 先用BookTestDao將資料寫死，等待BookDao完成只須改Service
    public class BookTestDao : IBookDao
    {
        public void DeleteBookById(string bookId)
        {
            throw new NotImplementedException();
        }

        public List<Book> GetBookDataByCondition(BookSearchArg arg)
        {
            List<LibraryManagementSystem.Model.Book> result = new List<LibraryManagementSystem.Model.Book>();
            result.Add(new LibraryManagementSystem.Model.Book()
            {
                BookId = 9999,
                BookKeeperId = "0012",
                BookClassName = "Database",
                BookName = "資料庫",
                BookBoughtDate = DateTime.ParseExact("2012/12/12", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                StatusName = "可以借出",
                BookKeeperName = ""
            });
            result.Add(new LibraryManagementSystem.Model.Book()
            {
                BookId = 9999,
                BookKeeperId = "",
                BookClassName = "Database",
                BookName = "資料庫",
                BookBoughtDate = DateTime.ParseExact("2012/12/12", "yyyy/MM/dd", CultureInfo.InvariantCulture),
                StatusName = "可以借出",
                BookKeeperName = ""
            });
            return result;
        }

        public Book GetBookDetailById(int bookId)
        {
            throw new NotImplementedException();
        }

        public List<BookSearchArg> GetBookIdNameData()
        {
            throw new NotImplementedException();
        }

        public List<BookLendRecord> GetBookLendRecord(int bookId)
        {
            throw new NotImplementedException();
        }

        public int InsertBook(Book book)
        {
            throw new NotImplementedException();
        }

        public void UpdateBookData(Book updateBookData, Book originBookData)
        {
            throw new NotImplementedException();
        }
    }
}
