﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LibraryManagementSystem.Models
{

    public class BookService
    {
        /// <summary>
        /// 取得DB連線字串
        /// </summary>
        /// <returns></returns>
        private string GetDBConnectionString()
        {
            return
                System.Configuration.ConfigurationManager.ConnectionStrings["DBConn"].ConnectionString.ToString();
        }
        

        /// <summary>
        /// 取得書名資料
        /// </summary>
        /// <returns></returns>
        public List<Models.BookSearchArg> GetBookIdNameData()
        {
            DataTable dt = new DataTable();
            string sql = @" SELECT bd.BOOK_ID AS BookId, bd.BOOK_NAME AS BookName
                            FROM BOOK_DATA AS bd
                            ORDER BY BookName";

            using (SqlConnection conn = new SqlConnection(this.GetDBConnectionString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(cmd);
                sqlAdapter.Fill(dt);
                conn.Close();
            }
            return this.MapBookNameToList(dt);
        }

        /// <summary>
        /// 依照條件取得圖書資料
        /// </summary>
        /// <returns></returns>
        public List<Models.Book> GetBookDataByCondition(Models.BookSearchArg arg)
        {

            DataTable dt = new DataTable();
            // SQL語法JOIN
            string sql = @"SELECT bd.BOOK_ID AS BookId, mm.USER_ID AS BookKeeperId, bc.BOOK_CLASS_NAME AS BookClassName, bd.BOOK_NAME AS BookName, 
                                CONVERT(VARCHAR(10), bd.BOOK_BOUGHT_DATE, 111) AS BookBoughtDate, bc1.CODE_NAME AS StatusName, mm.USER_ENAME AS BookKeeperName
                            FROM BOOK_DATA as bd
                            INNER JOIN BOOK_CLASS bc ON bd.BOOK_CLASS_ID = bc.BOOK_CLASS_ID
                            INNER JOIN BOOK_CODE bc1 ON bd.BOOK_STATUS = bc1.CODE_ID AND bc1.CODE_TYPE='BOOK_STATUS'
                            LEFT JOIN MEMBER_M mm ON bd.BOOK_KEEPER = mm.USER_ID
                            WHERE (bd.BOOK_NAME LIKE '%' + @BookName + '%' OR @BookName='') AND
                                (bc.BOOK_CLASS_ID = @BookClassId OR @BookClassId='') AND
                                (mm.USER_ID = @BookKeeperId OR @BookKeeperId='') AND
	                            (bc1.CODE_ID = @BookStatusId OR @BookStatusId='')
                            ORDER BY BookBoughtDate DESC";

            using (SqlConnection conn = new SqlConnection(this.GetDBConnectionString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                // 可改寫方式 -> a ?? string.empty
                cmd.Parameters.Add(new SqlParameter("@BookName", arg.BookName == null ? string.Empty : arg.BookName));
                cmd.Parameters.Add(new SqlParameter("@BookClassId", arg.BookClassId == null ? string.Empty : arg.BookClassId));
                cmd.Parameters.Add(new SqlParameter("@BookKeeperId", arg.BookKeeperId == null ? string.Empty : arg.BookKeeperId));
                cmd.Parameters.Add(new SqlParameter("@BookStatusId", arg.BookStatusId == null ? string.Empty : arg.BookStatusId));
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(cmd);
                sqlAdapter.Fill(dt);
                conn.Close();
            }
            return this.MapBookDataToList(dt);
        }


        /// <summary>
        /// 取得圖書明細資料
        /// </summary>
        /// <returns></returns>
        public Models.Book GetBookDetailById(int bookId)
        {

            DataTable dt = new DataTable();
            string sql = @"SELECT bd.BOOK_ID AS BookId, BOOK_NAME AS BookName, bd.BOOK_AUTHOR AS BookAuthor, bd.BOOK_PUBLISHER AS BookPublisher, bd.BOOK_NOTE AS BookNote,
                                 CONVERT(VARCHAR(10), bd.BOOK_BOUGHT_DATE, 111) AS BookBoughtDate, bc.BOOK_CLASS_ID AS BookClassId, bc.BOOK_CLASS_NAME AS BookClassName,  
								 bc1.CODE_ID AS BookStatusId, bc1.CODE_NAME AS StatusName, bd.BOOK_KEEPER AS BookKeeperId, CONCAT( mm.USER_ENAME, '-', mm.USER_CNAME) AS BookKeeperName
                            FROM BOOK_DATA as bd
                            INNER JOIN BOOK_CLASS bc ON bd.BOOK_CLASS_ID = bc.BOOK_CLASS_ID
                            INNER JOIN BOOK_CODE bc1 ON bd.BOOK_STATUS = bc1.CODE_ID AND bc1.CODE_TYPE='BOOK_STATUS'
                            LEFT JOIN MEMBER_M mm ON bd.BOOK_KEEPER = mm.USER_ID
                            WHERE bd.BOOK_ID = @BookId";

            using (SqlConnection conn = new SqlConnection(this.GetDBConnectionString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.Add(new SqlParameter("@BookId", bookId));
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(cmd);
                sqlAdapter.Fill(dt);
                conn.Close();
            }
            return this.MapBookDetail(dt);
        }


        /// <summary>
        /// 新增圖書資料
        /// </summary>
        /// <param name="book"></param>
        /// <returns></returns>
        public int InsertBook(Models.Book book)
        {
            // SELECT SCOPE_IDENTITY() 取得識別欄位的最大值

            string sql = @" BEGIN TRANSACTION
                            BEGIN TRY
                                INSERT INTO BOOK_DATA
                                (
        	                        BOOK_NAME, BOOK_AUTHOR, BOOK_PUBLISHER, BOOK_NOTE, BOOK_BOUGHT_DATE, 
			                        BOOK_CLASS_ID, BOOK_STATUS, BOOK_KEEPER, BOOK_AMOUNT, CREATE_DATE,
			                        CREATE_USER, MODIFY_DATE, MODIFY_USER
                                )
                                VALUES
                                (
        	                        @BookName,@BookAuthor, @BookPublisher, @BookNote, @BookBoughtDate,
			                        @BookClassId, @BookStatusId, '', '10', GETDATE(), 
			                        '999', GETDATE(), '999'
                                )
                                SELECT SCOPE_IDENTITY()
                            END TRY

                            BEGIN CATCH
	                            SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity,
		                            ERROR_STATE() as ErrorState, ERROR_PROCEDURE() as ErrorProcedure,
		                            ERROR_LINE() as ErrorLine, ERROR_MESSAGE() as ErrorMessage;

	                            IF @@TRANCOUNT > 0
                                ROLLBACK TRANSACTION;
                            END CATCH;

                            IF @@TRANCOUNT > 0
                            COMMIT TRANSACTION;";
            int bookId; 
            using (SqlConnection conn = new SqlConnection(this.GetDBConnectionString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.Add(new SqlParameter("@BookName", book.BookName));
                cmd.Parameters.Add(new SqlParameter("@BookAuthor", book.BookAuthor));
                cmd.Parameters.Add(new SqlParameter("@BookPublisher", book.BookPublisher));
                cmd.Parameters.Add(new SqlParameter("@BookNote", book.BookNote));
                cmd.Parameters.Add(new SqlParameter("@BookBoughtDate", book.BookBoughtDate));
                cmd.Parameters.Add(new SqlParameter("@BookClassId", book.BookClassId));
                cmd.Parameters.Add(new SqlParameter("@BookStatusId", book.BookStatusId));
                bookId = Convert.ToInt32(cmd.ExecuteScalar());
                conn.Close();
            }
            return bookId;
        }


        /// <summary>
        /// 編輯圖書資料(修改)
        /// </summary>
        /// <returns></returns>
        public void UpdateBookData(Models.Book updateBookData, Models.Book originBookData)
        {
            //共通sql前段(TRY)
            string sql = @" BEGIN TRANSACTION
                            BEGIN TRY
                                UPDATE BOOK_DATA
	                            SET BOOK_NAME = @BookName, BOOK_CLASS_ID = @BookClassId, BOOK_AUTHOR = @BookAuthor,
		                            BOOK_BOUGHT_DATE = @BookBoughtDate,	BOOK_PUBLISHER = @BookPublisher, BOOK_NOTE = @BookNote,
		                            BOOK_STATUS = @BookStatusId, BOOK_KEEPER = @BookKeeperId, MODIFY_DATE = GETDATE(), MODIFY_USER = '999'
	                            WHERE BOOK_ID = @BookId;";
            string updateLendRecordSql = "";
            using (SqlConnection conn = new SqlConnection(this.GetDBConnectionString()))
            {

                // 借閱狀態若有變動
                if (!(originBookData.BookStatusId.Equals(updateBookData.BookStatusId)))
                {
                    // 借閱狀態變動為A(可以借出)，不變動借閱紀錄
                    // 借閱狀態變動為B(已借出)，新增該筆BookLendRecord
                    if (updateBookData.BookStatusId.Equals("B"))
                    {
                        updateLendRecordSql = @" INSERT INTO BOOK_LEND_RECORD( KEEPER_ID, BOOK_ID, LEND_DATE, CRE_DATE, CRE_USR, MOD_DATE, MOD_USR)
	                                        SELECT @BookKeeperId AS KEEPER_ID, @BookId AS BOOK_ID, GETDATE() AS LEND_DATE, GETDATE() AS CRE_DATE, @BookKeeperId AS CRE_USR
		                                    , GETDATE() AS MOD_DATE, @BookKeeperId AS MOD_USR;";
                        sql = sql + updateLendRecordSql;
                    }
                }
                //共通sql後段(CATCH+COMMIT)
                sql = sql + @" END TRY

                                BEGIN CATCH
	                                SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity,
		                                ERROR_STATE() as ErrorState, ERROR_PROCEDURE() as ErrorProcedure,
		                                ERROR_LINE() as ErrorLine, ERROR_MESSAGE() as ErrorMessage;

	                                IF @@TRANCOUNT > 0
                                    ROLLBACK TRANSACTION;
                                END CATCH;

                                IF @@TRANCOUNT > 0
                                COMMIT TRANSACTION;";
                SqlCommand cmd = new SqlCommand(sql, conn);
                conn.Open();
                cmd.Parameters.Add(new SqlParameter("@BookName", updateBookData.BookName));
                cmd.Parameters.Add(new SqlParameter("@BookAuthor", updateBookData.BookAuthor));
                cmd.Parameters.Add(new SqlParameter("@BookPublisher", updateBookData.BookPublisher));
                cmd.Parameters.Add(new SqlParameter("@BookNote", updateBookData.BookNote));
                cmd.Parameters.Add(new SqlParameter("@BookBoughtDate", updateBookData.BookBoughtDate));
                cmd.Parameters.Add(new SqlParameter("@BookClassId", updateBookData.BookClassId));
                cmd.Parameters.Add(new SqlParameter("@BookId", updateBookData.BookId));
                cmd.Parameters.Add(new SqlParameter("@BookStatusId", updateBookData.BookStatusId));
                cmd.Parameters.Add(new SqlParameter("@BookKeeperId", updateBookData.BookKeeperId == null ? string.Empty : updateBookData.BookKeeperId));
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }


        /// <summary>
        /// 刪除圖書資料
        /// </summary>
        public void DeleteBookById(string bookId)
        {
            try
            {
                // 刪除圖書資料也要一併刪除借閱紀錄
                string sql = @" BEGIN TRANSACTION
                                BEGIN TRY
                                    DELETE FROM BOOK_DATA Where BOOK_ID=@BookId
                                    DELETE FROM BOOK_LEND_RECORD WHERE BOOK_ID = @BookId
                                END TRY

                                BEGIN CATCH
	                                SELECT ERROR_NUMBER() AS ErrorNumber, ERROR_SEVERITY() AS ErrorSeverity,
		                                ERROR_STATE() as ErrorState, ERROR_PROCEDURE() as ErrorProcedure,
		                                ERROR_LINE() as ErrorLine, ERROR_MESSAGE() as ErrorMessage;

	                                IF @@TRANCOUNT > 0
                                    ROLLBACK TRANSACTION;
                                END CATCH;

                                IF @@TRANCOUNT > 0
                                COMMIT TRANSACTION;";
                using (SqlConnection conn = new SqlConnection(this.GetDBConnectionString()))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(sql, conn);
                    cmd.Parameters.Add(new SqlParameter("@BookId", bookId));
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// 取得圖書明細資料
        /// </summary>
        /// <returns></returns>
        public List<Models.BookLendRecord> GetBookLendRecord(int bookId)
        {

            DataTable dt = new DataTable();
            string sql = @" SELECT blr.BOOK_ID AS BookId, CONVERT(VARCHAR(10),  blr.LEND_DATE, 111) AS LendDate, blr.KEEPER_ID AS BookKeeperId, mm.USER_ENAME AS BookKeeperName, mm.USER_CNAME AS BookKeeperCName
                            FROM BOOK_LEND_RECORD blr
                            INNER JOIN MEMBER_M mm ON blr.KEEPER_ID = mm.USER_ID
                            WHERE blr.BOOK_ID = @BookId
							ORDER BY LendDate DESC
                            ";

            using (SqlConnection conn = new SqlConnection(this.GetDBConnectionString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.Add(new SqlParameter("@BookId", bookId));
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(cmd);
                sqlAdapter.Fill(dt);
                conn.Close();
            }
            return this.MapBookLendRecordToList(dt);
        }


        /// <summary>
        /// Map圖書資料進List
        /// </summary>
        /// <param name="bookData"></param>
        /// <returns></returns>
        private List<Models.Book> MapBookDataToList(DataTable bookData)
        {
            List<Models.Book> result = new List<Book>();
            foreach (DataRow row in bookData.Rows)
            {
                result.Add(new Book()
                {
                    BookId = (int)row["BookId"],
                    BookKeeperId = row["BookKeeperId"].ToString(),
                    BookClassName = row["BookClassName"].ToString(),
                    BookName = row["BookName"].ToString(),
                    BookBoughtDate = DateTime.ParseExact(row["BookBoughtDate"].ToString(), "yyyy/MM/dd", CultureInfo.InvariantCulture),
                    StatusName = row["StatusName"].ToString(),
                    BookKeeperName = row["BookKeeperName"].ToString()
                });
            }
            return result;
        }


        /// <summary>
        /// Map圖書明細資料
        /// </summary>
        /// <param name="bookData"></param>
        /// <returns></returns>
        private Models.Book MapBookDetail(DataTable bookData)
        {
            Models.Book result = new Book();
            foreach (DataRow row in bookData.Rows)
            {
                result.BookId = (int)row["BookId"];
                result.BookKeeperId = row["BookKeeperId"].ToString();
                result.BookClassName = row["BookClassName"].ToString();
                result.BookName = row["BookName"].ToString();
                result.BookBoughtDate = DateTime.ParseExact(row["BookBoughtDate"].ToString(), "yyyy/MM/dd", CultureInfo.InvariantCulture);
                result.StatusName = row["StatusName"].ToString();
                result.BookKeeperName = row["BookKeeperName"].ToString();
                result.BookAuthor = row["BookAuthor"].ToString();
                result.BookPublisher = row["BookPublisher"].ToString();
                result.BookNote = row["BookNote"].ToString();
                result.BookClassId = row["BookClassId"].ToString();
                result.BookStatusId = row["BookStatusId"].ToString();
            }
            return result;
        }


        /// <summary>
        /// Map圖書借閱紀錄資料
        /// </summary>
        /// <param name="bookData"></param>
        /// <returns></returns>
        private List<Models.BookLendRecord> MapBookLendRecordToList(DataTable bookData)
        {
            List<Models.BookLendRecord> result = new List<Models.BookLendRecord>();
            foreach (DataRow row in bookData.Rows)
            {
                result.Add(new BookLendRecord()
                {
                    BookId = (int)row["BookId"],
                    LendDate = DateTime.ParseExact(row["LendDate"].ToString(), "yyyy/MM/dd", CultureInfo.InvariantCulture),
                    BookKeeperId = row["BookKeeperId"].ToString(),
                    BookKeeperName = row["BookKeeperName"].ToString(),
                    BookKeeperCName = row["BookKeeperCName"].ToString()
                });
            }
            return result;
        }


        /// <summary>
        /// Map書名資料
        /// </summary>
        /// <param name="bookData"></param>
        /// <returns></returns>
        private List<Models.BookSearchArg> MapBookNameToList(DataTable bookData)
        {
            List<Models.BookSearchArg> result = new List<Models.BookSearchArg>();
            foreach (DataRow row in bookData.Rows)
            {
                result.Add(new BookSearchArg()
                {
                    BookId = (int)row["BookId"],
                    BookName = row["BookName"].ToString(),
                });
            }
            return result;
        }
    }
}