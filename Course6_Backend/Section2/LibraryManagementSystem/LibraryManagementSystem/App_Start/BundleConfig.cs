﻿using System.Web.Optimization;

namespace LibraryManagementSystem
{
    public class BundleConfig
    {
        // 如需統合的詳細資訊，請瀏覽 https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/lib/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/lib/jquery.validate*"));

            // 使用開發版本的 Modernizr 進行開發並學習。然後，當您
            // 準備好可進行生產時，請使用 https://modernizr.com 的建置工具，只挑選您需要的測試。
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/lib/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/lib/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/style.css",
                      "~/Content/kendo.material-main.min.css",
                      "~/Content/kendo.common-material.min.css",
                      "~/Content/uikit.min.css"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                      "~/Scripts/lib/kendo.all.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/uikit").Include(
                      "~/Scripts/lib/uikit.min.js"));

        }
    }
}
