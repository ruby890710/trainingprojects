﻿//初始化kendo validate
var validator = InitValidator("#BookDataForm");
function InitValidator(id) {
    return $(id).kendoValidator({
        messages: {
            required: "不可為空白",
            datevalidation: function (input) {
                // Return the message text.
                return input.attr("data-val-datevalidation");
            }
        },
        rules: {
            // 自訂 date validation.
            dateValidation: function (input, params) {
                if (input.is("[name='book bought date']") && input.val() != "") {
                    if (new Date(input.val()) > new Date()) {
                        input.attr("data-datevalidation-msg", "您輸入未來日期，請重新輸入正確購買日期");
                        return false;
                    }
                    var date = kendo.parseDate(input.val(), "yyyy-MM-dd");
                    input.attr("data-datevalidation-msg", "日期不存在或日期格式錯誤(yyyy-MM-dd)");
                    if (date) {
                        return true;
                    }
                    return false;
                }
                return true;
            }
        }
    });
}

$(document).ready(function () {
    //-----------------------Index-----------------------
    // 書名欄位(kendoAutoComplete)
    $("#BookName").kendoAutoComplete({
        placeholder: "請輸入書名關鍵字...",
        dataTextField: "BookName",
        dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: "/Book/GetBookNameData",
                    type: "post",
                    dataType: "json"
                }
            }
        })
    });

    // 圖書類別欄位(kendoDropDownList)
    GetDropDownListCodeData("#BookClassId", "/Book/GetBookClassCodeData","請選擇圖書類別...");

    // 借閱人欄位(kendoDropDownList)
    GetDropDownListCodeData("#BookKeeperId", "/Book/GetBookKeeperCodeData", "請選擇借閱人...");

    // 借閱狀態(kendoDropDownList)
    GetDropDownListCodeData("#BookStatusId", "/Book/GetBookStatusCodeData", "請選擇借閱狀態...");

    // 圖書查詢結果(kendoGrid)
    $("#BookSearchResult").kendoGrid({
        dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: "/Book/Index",
                    type: "post",
                    dataType: "json",
                    data: function () {
                        var data = {
                            "BookName": $("#BookName").val(),
                            "BookClassId": $("#BookClassId").data("kendoDropDownList").value(),
                            "BookKeeperId": $("#BookKeeperId").data("kendoDropDownList").value(),
                            "BookStatusId": $("#BookStatusId").data("kendoDropDownList").value()
                        };
                        return data
                    }
                }
            },
            schema: {
                model: {
                    fields: {
                        BookClassName: { type: "string" },
                        BookName: { type: "string" },
                        BookBoughtDate: { type: "string" },
                        StatusName: { type: "string" },
                        BookKeeperName: { type: "string" }
                    }
                }
            },
        }),
        height: 550,
        columns: [
            { field: "BookClassName", title: "圖書類別", width: "13%" },
            {
                field: "BookName", title: "書名", width: "30%",
                // template: " <a onclick = 'OpenBookDetailWindow(#: BookId #,event)'>#: BookName #</a>"
                template: " <a onclick = 'OpenBookDetailWindow(event)'>#: BookName #</a>"
            },
            { field: "BookBoughtDate", title: "購書日期", width: "10%", template: "#= kendo.toString(kendo.parseDate(BookBoughtDate, 'yyyy-MM-dd'), 'yyyy/MM/dd') #" },
            { field: "StatusName", title: "借閱狀態", width: "10%" },
            { field: "BookKeeperName", title: "借閱人", width: "7%" },
            { command: { text: "借閱紀錄", click: OpenBookLendRecordWindow }, title: " ", width: "10%" },            
            { command: { text: "編輯", click: OpenUpdateBookWindow }, title: " ", width: "8%" },
            { command: { text: "刪除", click: DeleteBook }, title: " ", width: "8%" }
        ]
    });

    // 查詢按鈕
    $("#BookSearch").on("click", function () {
        $("#BookSearchResult").data("kendoGrid").dataSource.read();
    });

    // 清除按鈕
    $("#ClearSearchArg").on("click", function () {
        $("#BookName").val("");
        $("#BookClassId").data("kendoDropDownList").select("");
        $("#BookStatusId").data("kendoDropDownList").select("");
        $("#BookKeeperId").data("kendoDropDownList").select("");
        $("#BookSearchResult").data("kendoGrid").dataSource.read();
    })

    // 新增按鈕
    $("#InsertBookWindow").on("click", function () {
        InitBookDataWindow("InsertBookWindow");
    })
    //-----------------------Index-----------------------

    //-----------------------Insert、Update、Delete-----------------------
    //內容簡介欄位(kendoTextArea)
    $("#BookNoteData").kendoTextArea({
        rows: 5,
        maxLength: 1000,
        resizable:"both",
        placeholder: "請輸入圖書內容簡介"
    });
    //內容簡介字數計算
    $("#BookNoteData").on('input', function (e) {
        $('.k-counter-container .k-counter-value').html($(e.target).val().length);
    });

    //購買日期(kendoDatePicker)
    $("#BookBoughtDateData").kendoDatePicker({
        format: "yyyy-MM-dd",
        max: "9999/12/31"
    });

    // 圖書類別欄位(kendoDropDownList)
    GetDropDownListCodeData("#BookClassData", "/Book/GetBookClassCodeData", "請選擇圖書類別...");

    // 借閱狀態(kendoDropDownList)
    GetDropDownListCodeData("#BookStatusData", "/Book/GetBookStatusCodeData", "請選擇借閱狀態...");
    $("#BookStatusData").data("kendoDropDownList").bind("change",OnBookStatusDataChange)
    function OnBookStatusDataChange() {
        SetBookKeeperReadonlyByBookStatus();
     }  

    // 借閱人欄位(kendoDropDownList)
    GetDropDownListCodeData("#BookKeeperData", "/Book/GetBookKeeperCodeData", "請選擇借閱人...");

    // InsertBookWindow新增按鈕
    $("#InsertBook").on("click", function (e) {
        e.preventDefault();
        InsertBook();
    })

    // OpenUpdateBookWindow存檔按鈕(編輯)
    $("#UpdateBook").on("click", function (e) {
        e.preventDefault();
        UpdateBook();
    });
    //-----------------------Insert、Update、Delete-----------------------

    //-----------------------BookLendRecord-----------------------
    $("#BookLendRecordWindow").kendoWindow({
        visible: false,
        width: "40%",
        height: "90%",
        title: "借閱紀錄",
    });
    //-----------------------BookLendRecord-----------------------     


})

//初始化kendo window( WindowAction : 以何種格式開啟window )
function InitBookDataWindow(WindowAction) {
    $("#BookDataWindow").kendoWindow({
        visible: false,
        width: "40%",
        height: "90%",
        title: "書籍資料",
    });
    var BookBoughtDate = $("#BookBoughtDateData");
    var BookClassId = $("#BookClassData");
    var BookStatusId = $("#BookStatusData");
    var BookKeeperId = $("#BookKeeperData");

    //重置kendoWindow    
    $(".bookdata-text").val("");
    // BookNote字數設定
    $('.k-counter-container .k-counter-value').html(0);
    BookBoughtDate.data("kendoDatePicker").value(new Date());
    BookClassId.data("kendoDropDownList").select(0);
    BookStatusId.data("kendoDropDownList").value("A");
    BookKeeperId.data("kendoDropDownList").select(0);
    // 不顯示validate
    $("#BookDataForm").kendoValidator().data("kendoValidator").hideMessages();
    // 設定全部欄位為可編輯
    $(".bookdata-text").attr("readonly", false);
    BookBoughtDate.data("kendoDatePicker").readonly(false);
    // jQuery遍歷: $( selector).each( function(indexInArray, valueOfElement))
    //indexInArray - 選擇器的 index 位置，valueOfElement - 當前的元素（也可用"this"選擇器）
    $.each($("select.bookdata-dropdownlist"), function (indexInArray, valueOfElement) {
        $(valueOfElement).data("kendoDropDownList").readonly(false);
    });

    // 隱藏送出按鈕
    $(".bookdata-submit").hide();
    //隱藏借閱狀態及借閱人欄位
    $(".bookdata-column").hide();

    switch (WindowAction) {
        // 新增window
        case "InsertBookWindow":
            // 顯示新增按鈕
            $("#InsertBook").show();
            break;
        case "OpenBookDetailWindow":
            //設定欄位皆為不可編輯
            $(".bookdata-text").attr("readonly", true);
            BookBoughtDate.data("kendoDatePicker").readonly();
            $.each($("select.bookdata-dropdownlist"), function (indexInArray, valueOfElement) {
                $(valueOfElement).data("kendoDropDownList").readonly();
            });
            //顯示借閱狀態及借閱人
            $(".bookdata-column").show();
            break;
        case "OpenUpdateBookWindow":
            // 顯示存檔按鈕
            $("#UpdateBook").show();
            //顯示借閱狀態及借閱人
            $(".bookdata-column").show();
            break;
    }

    //欄位驗證validate
    validator = InitValidator("#BookDataForm");
    
    var kendo_window = $("#BookDataWindow").data("kendoWindow");
    //置中開啟kendo window
    kendo_window.center().open();
}

//新增書籍
function InsertBook() {
    //---------------------新增書籍資料---------------------
    //檢查validator
    if (validator.data("kendoValidator").validate()) {
        var BookClassId = $("#BookClassData").data("kendoDropDownList").value();
        var BookName = $("#BookNameData").val();
        var BookAuthor = $("#BookAuthorData").val();
        var BookPublisher = $("#BookPublisherData").val();
        var BookNote = $("#BookNoteData").val();
        var BookBoughtDate = $("#BookBoughtDateData").data("kendoDatePicker").value();
        BookBoughtDate = kendo.toString(BookBoughtDate, "yyyy-MM-dd");
        var data = {
            "BookName": BookName,
            "BookAuthor": BookAuthor,
            "BookPublisher": BookPublisher,
            "BookNote": BookNote,
            "BookBoughtDate": BookBoughtDate,
            "BookClassId": BookClassId,
            "BookStatusId": "A"
        };
        //新增圖書至資料庫
        var isInserted = function (response) {
            if (response.status) {
                    var NewBookId = response.newBookId;
                    console.log(NewBookId)
                    var newObject = {
                        "BookId": NewBookId,
                        "BookName": BookName,
                        "BookAuthor": BookAuthor,
                        "BookPublisher": BookPublisher,
                        "BookNote": BookNote,
                        "BookBoughtDate": BookBoughtDate,
                        "BookClassId": BookClassId,
                        "BookStatusId": "A"
                    }
                    var kendoGrid = $("#BookSearchResult").data("kendoGrid");
                    //新增成功後，關閉kendo window
                    $("#BookDataWindow").data("kendoWindow").close();
                    kendoGrid.dataSource.add(newObject);
                    kendoGrid.dataSource.read();
                    $("#BookName").data("kendoAutoComplete").dataSource.read();
                    //顯示新增成功提示
                    $('#msg').css("color", "green");
                    $('#msg').html(response.message);
                    $('#msg').delay(1000).fadeOut('slow');
                }
                else {
                    alert(response.message);
                }
        }
        AjaxTool("/Book/InsertBook", data, isInserted)
    }
    else {
        alert("格式錯誤，無法新增");
    }
}

//書籍明細(kendoWindow)
function OpenBookDetailWindow(e) {
    //取消目標元素的預設行為
    e.preventDefault();
    InitBookDataWindow("OpenBookDetailWindow");

    var grid = $("#BookSearchResult").data("kendoGrid");
    //取得使用者選取列
    var getItem = grid.dataItem($(e.target).closest("tr"));

    // 取得圖書明細
    var getDetailSuccess = function (response) {
        var BookData = response;
            $("#BookNameData").val(BookData.BookName);
            $("#BookAuthorData").val(BookData.BookAuthor);
            $("#BookPublisherData").val(BookData.BookPublisher);
            $("#BookNoteData").val(BookData.BookNote);
            $("#BookBoughtDateData").data("kendoDatePicker").value(BookData.BookBoughtDate);
            $("#BookClassData").data("kendoDropDownList").value(BookData.BookClassId);
            $("#BookStatusData").data("kendoDropDownList").value(BookData.BookStatusId)
            $("#BookKeeperData").data("kendoDropDownList").value(BookData.BookKeeperId);
            // BookNote字數設定
            $('.k-counter-container .k-counter-value').html($("#BookNoteData").val().length);
    }
    AjaxTool("/Book/BookDetail", "bookId=" + getItem.get("BookId"), getDetailSuccess)
}

//借閱紀錄(kendoWindow)
function OpenBookLendRecordWindow(e) {
    //取消目標元素的預設行為
    e.preventDefault();

    var grid = $("#BookSearchResult").data("kendoGrid");
    //取得使用者選取列
    var getItem = grid.dataItem($(e.target).closest("tr"));

    // 圖書借閱紀錄(kendoGrid)
    $("#BookLendRecordGrid").kendoGrid({
        dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    type: "POST",
                    url: "/Book/BookLendRecord",
                    data: function () { return { "BookId": getItem.get("BookId") } },
                    dataType: "json"
                }
            },
            schema: {
                model: {
                    fields: {
                        BookClassName: { type: "string" },
                        BookName: { type: "string" },
                        BookBoughtDate: { type: "string" },
                        StatusName: { type: "string" },
                        BookKeeperName: { type: "string" }
                    }
                }
            },
        }),
        height: 550,
        columns: [
            { field: "LendDate", title: "借閱日期", width: "10%", template: "#= kendo.toString(kendo.parseDate(LendDate, 'yyyy-MM-dd'), 'yyyy/MM/dd') #" },
            { field: "BookKeeperId", title: "借閱人員編號", width: "7%" },
            { field: "BookKeeperName", title: "英文姓名", width: "7%" },
            { field: "BookKeeperCName", title: "中文姓名", width: "7%" }
        ]
    });
    var kendo_window = $("#BookLendRecordWindow").data("kendoWindow");
    //置中開啟kendo window
    kendo_window.center().open();
}

// 編輯書籍(kendoWindow)
function OpenUpdateBookWindow(e) {
    //取消目標元素的預設行為
    e.preventDefault();
    InitBookDataWindow("OpenUpdateBookWindow");

    //取得使用者選取列
    var getItem = this.dataItem($(e.target).closest("tr"));

    // 取得圖書明細
    var getDetailSuccess = function (response) {
        var BookData = response;
            $("#BookIdData").val(BookData.BookId);
            $("#BookNameData").val(BookData.BookName);
            $("#BookAuthorData").val(BookData.BookAuthor);
            $("#BookPublisherData").val(BookData.BookPublisher);
            $("#BookNoteData").val(BookData.BookNote);
            $("#BookBoughtDateData").data("kendoDatePicker").value(BookData.BookBoughtDate);
            $("#BookClassData").data("kendoDropDownList").value(BookData.BookClassId);
            $("#BookStatusData").data("kendoDropDownList").value(BookData.BookStatusId)
            $("#BookKeeperData").data("kendoDropDownList").value(BookData.BookKeeperId);
            // BookNote字數設定
            $('.k-counter-container .k-counter-value').html($("#BookNoteData").val().length);

            //初始化借閱狀態判斷
            SetBookKeeperReadonlyByBookStatus()
    }
    AjaxTool("/Book/BookDetail", "bookId=" + getItem.get("BookId"), getDetailSuccess)
}

//編輯書籍
function UpdateBook() {
    if ($("#BookStatusData").data("kendoDropDownList").value() == "B" && $("#BookKeeperData").data("kendoDropDownList").value() == "") {
        alert("借閱狀態為：已借出，請輸入借閱人");
        return false;
    }
    if (!confirm("是否確定更新？")) {
        return false;
    }
    if (validator.data("kendoValidator").validate()) {
        
        var BookId = $("#BookIdData").val();
        var BookName = $("#BookNameData").val();
        var BookAuthor = $("#BookAuthorData").val();
        var BookPublisher = $("#BookPublisherData").val();
        var BookNote = $("#BookNoteData").val();
        var BookBoughtDate = $("#BookBoughtDateData").data("kendoDatePicker").value();
        BookBoughtDate = kendo.toString(BookBoughtDate, "yyyy-MM-dd");
        var BookClassId = $("#BookClassData").data("kendoDropDownList").value();
        var BookStatusId = $("#BookStatusData").data("kendoDropDownList").value();
        var BookKeeperId = $("#BookKeeperData").data("kendoDropDownList").value();
        var data = {
            "BookId": BookId,
            "BookName": BookName,
            "BookAuthor": BookAuthor,
            "BookPublisher": BookPublisher,
            "BookNote": BookNote,
            "BookBoughtDate": BookBoughtDate,
            "BookClassId": BookClassId,
            "BookStatusId": BookStatusId,
            "BookKeeperId": BookKeeperId
        };
        //編輯圖書更新資料庫
        var isUpdated = function (response) {
            if (response.status) {
                $("#BookDataWindow").data("kendoWindow").close();
                    $("#BookSearchResult").data("kendoGrid").dataSource.read();
                    $("#BookName").data("kendoAutoComplete").dataSource.read();

                    //顯示新增成功提示
                    $('#msg').css("color", "green");
                    $('#msg').html(response.message);
                    $('#msg').delay(1000).fadeOut('slow');
            }
            else {
                alert(response.message);
            }
        }
        AjaxTool("/Book/UpdateBook", data, isUpdated)
    }
    else {
        alert("格式錯誤，無法新增");
    }
}


//刪除書籍(input-button)
function DeleteBook(e) {
    //取消目標元素的預設行為
    e.preventDefault();

    //取得使用者選取的刪除列
    var getItem = this.dataItem($(e.target).closest("tr"));
    console.log(getItem)
    //刪除confirm
    if (confirm("是否要刪除：(" + getItem.get("BookId") + ") " + getItem.get("BookName") + "?")) {
        var isDeleted = function (response) {
            if (response.status) {
                $("#BookSearchResult").data("kendoGrid").dataSource.remove(getItem);
                $("#BookName").data("kendoAutoComplete").dataSource.read();
                //顯示刪除成功提示
                $('#msg').css("color", "red");
                $('#msg').html(response.message).fadeIn('slow');
                $('#msg').delay(1000).fadeOut('slow');
            }
            else {
                alert(response.message);
            }
        }
        AjaxTool("/Book/DeleteBook", "bookId=" + getItem.get("BookId"), isDeleted)
    }
}


// 轉換顯示的日期格式
function ConvertDateFormat(value) {
    value = new Date(parseInt(value.replace("/Date(", "").replace(")/", ""), 10));
    return value;
}

// 設定借閱人是否為Readonly (由借閱狀態判斷)
function SetBookKeeperReadonlyByBookStatus() {
    var BookStatus = $("#BookStatusData").data("kendoDropDownList").value();
    if (BookStatus == "A" || BookStatus == "U") {
        $("#BookKeeperData").data("kendoDropDownList").readonly();
        $("#BookKeeperData").data("kendoDropDownList").value("");
    }
    else {
        $("#BookKeeperData").data("kendoDropDownList").readonly(false);
    }
}

/**
 * 取得dropdownlist的Code Data
 * @param {any} id - dropdownlist的id
 * @param {any} url - 取得Code Data的url
 * @param {any} optionLabel - optionlabel顯示內容
 */
function GetDropDownListCodeData(id, url, optionLabel) {
    $(id).kendoDropDownList({        
        dataTextField: "Text",
        dataValueField: "Value",
        dataSource: {
            transport: {
                read: {
                    url: url,   //"/Book/GetBookStatusCodeData",
                    type: "post",
                    dataType: "json"
                }
            }
        },
        optionLabel: optionLabel
    });
}

/**
 * 封裝ajax資料function
 * @param {any} url
 * @param {any} data
 * @param {function} successCallBack
 */
function AjaxTool(url, data, successCallBack) {
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType: "json",
        success: function (response) {
            successCallBack(response)
        }, error: function (error) {
            alert("系統發生錯誤，操作失敗");
        }
    });
}