﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using LibraryManagementSystem.Service;

namespace LibraryManagementSystem.Controllers
{
    public class BookController : Controller
    {
        // 操作圖書資料Service
        private ICodeService codeService { get; set; }
        // 取得DropDownList的Text/Value
        private IBookService bookService { get; set; }

        /// <summary>
        /// 取得圖書類別的Text/Value
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetBookClassCodeData()
        {
            var result = this.codeService.GetBookClass();
            return this.Json(result);
        }


        /// <summary>
        /// 取得借閱人的Text/Value
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetBookKeeperCodeData()
        {
            var result = this.codeService.GetBookKeeper();
            return this.Json(result);
        }


        /// <summary>
        /// 取得借閱狀態的Text/Value
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetBookStatusCodeData()
        {
            var result = this.codeService.GetBookStatus();
            return this.Json(result);
        }


        /// <summary>
        /// 取得書名
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetBookNameData()
        {
            var result = this.bookService.GetBookIdNameData();
            return this.Json(result);
        }


        /// <summary>
        /// 圖書資料查詢畫面
        /// </summary>
        /// <returns></returns>
        // GET: Book
        public ActionResult Index()
        {
           try
            {
                 return View();   
            }
            catch (Exception ex)
            {
                LibraryManagementSystem.Common.Logger.Write(LibraryManagementSystem.Common.Logger.LogCategory.Error, ex.ToString());
                return View("Error");
            }
            
        }


        /// <summary>
        /// 查詢圖書資料
        /// </summary>
        /// <returns></returns>
        [HttpPost()]
        public JsonResult Index(LibraryManagementSystem.Model.BookSearchArg bookSearchArg)
        {
            // 以參數條件取得Book Data
            var result = this.bookService.GetBookDataByCondition(bookSearchArg);
            return this.Json(result);
        }


        /// <summary>
        /// 圖書明細資料畫面
        /// </summary>
        /// <param name="bookId"></param>
        /// <returns></returns>
        [HttpPost()]
        public JsonResult BookDetail(string bookId)
        {
            // bookId資料型態為int，須轉型
            var id = int.Parse(bookId);
            LibraryManagementSystem.Model.Book result = bookService.GetBookDetailById(id);
            return this.Json(result);
        }

        /// <summary>
        /// 圖書更新畫面(修改)
        /// </summary>
        /// <param name="bookId"></param>
        /// <returns></returns>
        [HttpGet()]
        public ActionResult UpdateBook(string bookId)
        {
            var id = int.Parse(bookId);
            LibraryManagementSystem.Model.Book result = bookService.GetBookDetailById(id);
            return View(result);
        }


        /// <summary>
        /// 更新(修改)圖書資料
        /// </summary>
        /// <param name="book"></param>
        /// <returns></returns>
        [HttpPost()]
        public JsonResult UpdateBook(LibraryManagementSystem.Model.Book book)
        {
            try { 
            LibraryManagementSystem.Model.Book origin = bookService.GetBookDetailById(book.BookId);
            // 若validation通過，才能更新圖書資料並返回Index頁面

                bookService.UpdateBookData(book, origin);
                return this.Json(new { status = true, message = "成功編輯：(" + book.BookId + ") " + book.BookName });
            }catch(Exception ex)
            {
                LibraryManagementSystem.Common.Logger.Write(LibraryManagementSystem.Common.Logger.LogCategory.Error, ex.ToString());
                return this.Json(new { status = false, message = "編輯失敗" });
            }
        }


        /// <summary>
        /// 刪除圖書
        /// </summary>
        /// <param name="bookId"></param>
        /// <returns></returns>
        [HttpPost()]
        public JsonResult DeleteBook(string bookId)
        {
            var id = int.Parse(bookId);
            try
            {
                LibraryManagementSystem.Model.Book result = bookService.GetBookDetailById(id);
                // 若借閱狀態為B(已借出)或C(已借出未領)，不可刪除書籍
                if (result.BookStatusId == "B" || result.BookStatusId == "C")
                {
                    return this.Json(new { status = false, message = "本圖書已借出，無法刪除" });
                }
                else
                {
                    bookService.DeleteBookById(bookId);
                    return this.Json(new {status = true, message = "成功刪除：("+bookId+") "+result.BookName });
                }
            }
            catch (Exception ex)
            {
                LibraryManagementSystem.Common.Logger.Write(LibraryManagementSystem.Common.Logger.LogCategory.Error, ex.ToString());
                return this.Json(false);
            }
        }


        /// <summary>
        /// 新增圖書畫面
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public ActionResult InsertBook()
        {
            return View(new LibraryManagementSystem.Model.Book());
        }

        /// <summary>
        /// 新增圖書
        /// </summary>
        /// <param name="book"></param>
        /// <returns></returns>
        [HttpPost()]
        public JsonResult InsertBook(LibraryManagementSystem.Model.Book book)
        {
            try
            {
                int newBookId = 0;
                newBookId = bookService.InsertBook(book);
                return this.Json(new { status = true, message = "成功新增：(" + newBookId + ") " + book.BookName , newBookId = newBookId});
            }
            catch (Exception ex)
            {
                LibraryManagementSystem.Common.Logger.Write(LibraryManagementSystem.Common.Logger.LogCategory.Error, ex.ToString());
                return this.Json(new { status = false, message = "新增失敗" });
            }            
        }


        /// <summary>
        /// 圖書借閱紀錄畫面
        /// </summary>
        /// <param name="bookId"></param>
        /// <returns></returns>
        [HttpPost()]
        public JsonResult BookLendRecord(string bookId)
        {
            var id = int.Parse(bookId);
            List<LibraryManagementSystem.Model.BookLendRecord> result = bookService.GetBookLendRecord(id);
            return this.Json(result);
        }
    }
}