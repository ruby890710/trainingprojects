﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LibraryManagementSystem.Model
{
    public class Book
    {
        /// <summary>
        /// 書號(BOOK_ID)
        /// </summary>
        ///[MaxLength(5)]
        [DisplayName("書號")]
        public int BookId { get; set; }

        /// <summary>
        /// 書名
        /// </summary>
        [DisplayName("書名")]
        //[Required(ErrorMessage = "此欄位必填")]
        public string BookName { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        [DisplayName("作者")]
        //[Required(ErrorMessage = "此欄位必填")]
        public string BookAuthor { get; set; }

        /// <summary>
        /// 出版商
        /// </summary>
        [DisplayName("出版商")]
        //[Required(ErrorMessage = "此欄位必填")]
        public string BookPublisher { get; set; }

        /// <summary>
        /// 內容簡介
        /// </summary>
        [MaxLength(1000, ErrorMessage = "限制字數需小於1000字")]
        [AllowHtml]
        [DisplayName("內容簡介")]
        //[Required(ErrorMessage = "此欄位必填")]
        public string BookNote { get; set; }

        /// <summary>
        /// 購書日期
        /// </summary>
        [DisplayName("購書日期")]
        //[Required(ErrorMessage = "此欄位必填")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime BookBoughtDate { get; set; }

        /// <summary>
        /// 圖書類別名稱
        /// </summary>
        [DisplayName("圖書類別名稱")]
        public string BookClassName { get; set; }
        /// <summary>
        /// 圖書類別
        /// </summary>
        [DisplayName("圖書類別")]
        //[Required(ErrorMessage = "此欄位必填")]
        public string BookClassId { get; set; }

        /// <summary>
        /// 借閱狀態名稱
        /// </summary>
        [DisplayName("借閱狀態名稱")]
        public string StatusName { get; set; }
        /// <summary>
        /// 借閱狀態
        /// </summary>
        [DisplayName("借閱狀態")]
        //[Required(ErrorMessage = "此欄位必填")]
        public string BookStatusId { get; set; }

        /// <summary>
        /// 借閱人名稱
        /// </summary>
        [DisplayName("借閱人名稱")]
        public string BookKeeperName { get; set; }
        /// <summary>
        /// 借閱人英文名
        /// </summary>
        [DisplayName("借閱人中文名")]
        public string BookKeeperCName { get; set; }
        /// <summary>
        /// 借閱人
        /// </summary>
        [DisplayName("借閱人")]
        public string BookKeeperId { get; set; }

        /// <summary>
        /// 購書金額
        /// </summary>
        [DisplayName("購書金額")]
        public string BookAmount { get; set; }

        /// <summary>
        /// 建立時間
        /// </summary>
        [DisplayName("建立時間")]
        public string CreateDate { get; set; }

        /// <summary>
        /// 建立使用者
        /// </summary>
        [DisplayName("建立使用者")]
        public string CreateUser { get; set; }

        /// <summary>
        /// 修改時間
        /// </summary>
        [DisplayName("修改時間")]
        public string ModifyDate { get; set; }

        /// <summary>
        /// 修改使用者
        /// </summary>
        [DisplayName("修改使用者")]
        public string ModifyUser { get; set; }
    }

}
