﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManagementSystem.Model
{
    public class BookLendRecord
    {
        /// <summary>
        /// 書號(BOOK_ID)
        /// </summary>
        ///[MaxLength(5)]
        [DisplayName("書號")]
        public int BookId { get; set; }

        /// <summary>
        /// 借閱日期
        /// </summary>
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("借閱日期")]
        public DateTime LendDate { get; set; }

        /// <summary>
        /// 借閱人員編號
        /// </summary>
        [DisplayName("借閱人員編號")]
        public string BookKeeperId { get; set; }

        /// <summary>
        /// 借閱人英文姓名
        /// </summary>
        [DisplayName("英文姓名")]
        public string BookKeeperName { get; set; }

        /// <summary>
        /// 借閱人中文姓名
        /// </summary>
        [DisplayName("中文姓名")]
        public string BookKeeperCName { get; set; }

    }

}
