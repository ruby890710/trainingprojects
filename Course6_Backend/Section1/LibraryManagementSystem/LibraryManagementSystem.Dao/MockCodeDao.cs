﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LibraryManagementSystem.Dao
{
    public class MockCodeDao : ICodeDao
    {
        private string rootCodeDataFilePath = @"C:\Users\ruby_yr_liu\.gss\Course6_Backend\LibraryManagementSystem\LibraryManagementSystem\BookDataTxt\";

        public List<SelectListItem> GetBookClass()
        {
            string bookClassFilePath = rootCodeDataFilePath + "BOOK_CLASS.txt";

            var lines = File.ReadAllLines(bookClassFilePath);
            List<SelectListItem> result = new List<SelectListItem>();
            string splitChar = "\t";

            foreach (var item in lines)
            {
                result.Add(new SelectListItem()
                {
                    Text = item.Split(splitChar.ToCharArray())[1],
                    Value = item.Split(splitChar.ToCharArray())[0]
                });
            }
            return result;
        }

        public List<SelectListItem> GetBookKeeper()
        {
            string memborFilePath = rootCodeDataFilePath + "MEMBER_M.txt";

            var lines = File.ReadAllLines(memborFilePath);
            List<SelectListItem> result = new List<SelectListItem>();
            string splitChar = "\t";

            foreach (var item in lines)
            {
                result.Add(new SelectListItem()
                {
                    Text = item.Split(splitChar.ToCharArray())[1],
                    Value = item.Split(splitChar.ToCharArray())[0]
                });
            }
            return result;
        }

        public List<SelectListItem> GetBookCodeStatus(string status)
        {
            string bookCodeFilePath = rootCodeDataFilePath + "BOOK_CODE.txt";

            var lines = File.ReadAllLines(bookCodeFilePath);
            List<SelectListItem> result = new List<SelectListItem>();
            string splitChar = "\t";

            foreach (var item in lines)
            {
                if (item.Split(splitChar.ToCharArray())[0] == status)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = item.Split(splitChar.ToCharArray())[3],
                        Value = item.Split(splitChar.ToCharArray())[1]
                    });
                }

            }
            return result;
        }

        public List<SelectListItem> GetBookStatus()
        {                    
            return GetBookCodeStatus("BOOK_STATUS");
        }
    }
}
