﻿using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LibraryManagementSystem.Service
{
    public class CodeService : ICodeService
    {
        private ICodeDao codeDao { get; set; }
        /// <summary>
        /// 取得圖書類別資料(id, name)
        /// </summary>
        /// <returns></returns>
        public List<SelectListItem> GetBookClass()
        {
            return codeDao.GetBookClass();
        }

        /// <summary>
        /// 取得借閱人資料(id, name)
        /// </summary>
        /// <returns></returns>
        public List<SelectListItem> GetBookKeeper()
        {
            return codeDao.GetBookKeeper();
        }

        /// <summary>
        /// 取得借閱狀態資料(id, name)
        /// </summary>
        /// <returns></returns>
        public List<SelectListItem> GetBookStatus()
        {
            return codeDao.GetBookStatus();
        }
    }
}
