﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LibraryManagementSystem.Models
{
    //準備下拉選單的value, text
    public class CodeService
    {
        /// <summary>
        /// 取得DB連線字串
        /// </summary>
        /// <returns></returns>
        private string GetDBConnectionString()
        {
            return
                System.Configuration.ConfigurationManager.ConnectionStrings["DBConn"].ConnectionString.ToString();
        }


        /// <summary>
        /// 取得圖書類別資料(id, name)
        /// </summary>
        /// <param name="from">傳入呼叫此方法之Action名稱</param>
        /// <returns></returns>
        public List<SelectListItem> GetBookClass(string from)
        {
            DataTable dt = new DataTable();
            string sql = @"SELECT BOOK_CLASS_ID AS CodeId, BOOK_CLASS_NAME AS CodeName
                            FROM BOOK_CLASS
							ORDER BY CodeId";
            using (SqlConnection conn = new SqlConnection(this.GetDBConnectionString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(cmd);
                sqlAdapter.Fill(dt);
                conn.Close();
            }
            // 依View所需資料，設定不同Text
            if (from.Equals("Index"))
            {
                return this.MapCodeData(dt);
            }
            else
            {
                return this.MapCodeDataDetail(dt);
            }
        }

        /// <summary>
        /// 取得借閱人資料(id, name)
        /// </summary>
        /// <param name="from">傳入呼叫此方法之Action名稱</param>
        /// <returns></returns>
        public List<SelectListItem> GetBookKeeper(string from)
        {
            DataTable dt = new DataTable();
            string sql = @"SELECT DISTINCT mm.USER_ID AS CodeId, CONCAT(mm.USER_ENAME, '-', mm.USER_CNAME) AS CodeName
                            FROM BOOK_LEND_RECORD blr
                            INNER JOIN MEMBER_M mm ON blr.KEEPER_ID=mm.USER_ID
							ORDER BY CodeId";
            using (SqlConnection conn = new SqlConnection(this.GetDBConnectionString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(cmd);
                sqlAdapter.Fill(dt);
                conn.Close();
            }
            if (from.Equals("Index"))
            {
                return this.MapCodeData(dt);
            }
            else
            {
                return this.MapCodeDataDetail(dt);
            }
        }

        /// <summary>
        /// 取得借閱狀態資料(id, name)
        /// </summary>
        /// <param name="from">傳入呼叫此方法之Action名稱</param>
        /// <returns></returns>
        public List<SelectListItem> GetBookStatus(string from)
        {
            DataTable dt = new DataTable();
            string sql = @"SELECT CODE_ID AS CodeId, CODE_NAME AS CodeName
                            FROM BOOK_CODE
							WHERE BOOK_CODE.CODE_TYPE= 'BOOK_STATUS'
							ORDER BY CodeId";
            using (SqlConnection conn = new SqlConnection(this.GetDBConnectionString()))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(cmd);
                sqlAdapter.Fill(dt);
                conn.Close();
            }
            if (from.Equals("Index"))
            {
                return this.MapCodeData(dt);
            }
            else
            {
                return this.MapCodeDataDetail(dt);
            }
        }

        /// <summary>
        /// Maping 查詢代碼資料
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private List<SelectListItem> MapCodeData(DataTable dt)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            foreach (DataRow row in dt.Rows)
            {
                result.Add(new SelectListItem()
                {
                    Text = row["CodeId"].ToString() + '-' + row["CodeName"].ToString(),
                    Value = row["CodeId"].ToString()
                });
            }
            return result;
        }


        /// <summary>
        /// Maping 明細代碼資料
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private List<SelectListItem> MapCodeDataDetail(DataTable dt)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            foreach (DataRow row in dt.Rows)
            {
                result.Add(new SelectListItem()
                {
                    Text = row["CodeName"].ToString(),
                    Value = row["CodeId"].ToString()
                });
            }
            return result;
        }
    }
}