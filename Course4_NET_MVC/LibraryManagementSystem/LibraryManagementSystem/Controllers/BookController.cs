﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LibraryManagementSystem.Controllers
{
    public class BookController : Controller
    {
        /// <summary>
        /// 圖書資料查詢畫面
        /// </summary>
        /// <returns></returns>
        // GET: Book
        public ActionResult Index()
        {
            Models.CodeService codeService = new Models.CodeService();
            ViewBag.BookClassCodeData = codeService.GetBookClass("Index");
            ViewBag.BookKeeperCodeData = codeService.GetBookKeeper("Index");
            ViewBag.BookStatusCodeData = codeService.GetBookStatus("Index");
            return View();
        }


        /// <summary>
        /// 查詢圖書資料
        /// </summary>
        /// <returns></returns>
        [HttpPost()]
        public ActionResult Index(Models.BookSearchArg arg)
        {
            Models.CodeService codeService = new Models.CodeService();
            Models.BookService bookService = new Models.BookService();
            // CodeData要再Get一次，否則會找不到
            ViewBag.BookClassCodeData = codeService.GetBookClass("Index");
            ViewBag.BookKeeperCodeData = codeService.GetBookKeeper("Index");
            ViewBag.BookStatusCodeData = codeService.GetBookStatus("Index");
            // 以參數條件取得Book Data
            ViewBag.SearchResult = bookService.GetBookDataByCondition(arg);
            return View("Index");
        }


        /// <summary>
        /// 圖書明細資料畫面
        /// </summary>
        /// <param name="bookId"></param>
        /// <returns></returns>
        [HttpGet()]
        public ActionResult BookDetail(string bookId)
        {
            // bookId資料型態為int，須轉型
            var id = int.Parse(bookId);
            Models.BookService bookService = new Models.BookService();
            Models.Book result = bookService.GetBookDetail(id);
            return View(result);
        }

        /// <summary>
        /// 圖書更新畫面(修改)
        /// </summary>
        /// <param name="bookId"></param>
        /// <returns></returns>
        [HttpGet()]
        public ActionResult UpdateBook(string bookId)
        {
            Models.CodeService codeService = new Models.CodeService();
            ViewBag.BookClassCodeData = codeService.GetBookClass("UpdateBook");
            ViewBag.BookKeeperCodeData = codeService.GetBookKeeper("UpdateBook");
            ViewBag.BookStatusCodeData = codeService.GetBookStatus("UpdateBook");

            var id = int.Parse(bookId);
            Models.BookService bookService = new Models.BookService();
            Models.Book result = bookService.GetBookDetail(id);
            return View(result);
        }


        /// <summary>
        /// 更新(修改)圖書資料
        /// </summary>
        /// <param name="book"></param>
        /// <returns></returns>
        [HttpPost()]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateBook(Models.Book book)
        {
            Models.BookService bookService = new Models.BookService();
            Models.Book origin = bookService.GetBookDetail(book.BookId);
            Models.CodeService codeService = new Models.CodeService();
            ViewBag.BookClassCodeData = codeService.GetBookClass("UpdateBook");
            ViewBag.BookKeeperCodeData = codeService.GetBookKeeper("UpdateBook");
            ViewBag.BookStatusCodeData = codeService.GetBookStatus("UpdateBook");
            // 若validation通過，才能更新圖書資料並返回Index頁面
            if (ModelState.IsValid)
            {
                bookService.UpdateBookData(book, origin);
                TempData["alertMsg"] = "已更新：(" + book.BookId + ")" + book.BookName;
                ModelState.Clear();
                return RedirectToAction("Index");
            }
            return View();
        }


        /// <summary>
        /// 刪除圖書
        /// </summary>
        /// <param name="bookId"></param>
        /// <returns></returns>
        [HttpPost()]
        public JsonResult DeleteBook(string bookId)
        {
            var id = int.Parse(bookId);
            try
            {
                Models.BookService bookService = new Models.BookService();
                Models.Book result = bookService.GetBookDetail(id);
                // 若借閱狀態為B(已借出)或C(已借出未領)，不可刪除書籍
                if (result.BookStatusId == "B" || result.BookStatusId == "C")
                {
                    return this.Json(false);
                }
                else
                {
                    bookService.DeleteBookById(bookId);
                    return this.Json(true);
                }

            }

            catch (Exception ex)
            {
                return this.Json(false);
            }
        }


        /// <summary>
        /// 新增圖書畫面
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public ActionResult InsertBook()
        {
            Models.CodeService codeService = new Models.CodeService();
            ViewBag.BookClassCodeData = codeService.GetBookClass("InsertBook");
            ViewBag.BookStatusCodeData = codeService.GetBookStatus("InsertBook");
            return View(new Models.Book());
        }

        /// <summary>
        /// 新增圖書
        /// </summary>
        /// <param name="book"></param>
        /// <returns></returns>
        [HttpPost()]
        public ActionResult InsertBook(Models.Book book)
        {
            Models.CodeService codeService = new Models.CodeService();
            ViewBag.BookClassCodeData = codeService.GetBookClass("InsertBook");
            ViewBag.BookStatusCodeData = codeService.GetBookStatus("InsertBook");

            if (ModelState.IsValid)
            {
                Models.BookService bookService = new Models.BookService();
                int newBookId = bookService.InsertBook(book);
                TempData["alertMsg"] = "已新增：(" + newBookId + ")" + book.BookName;
                ModelState.Clear();
            }
            return View();
        }


        /// <summary>
        /// 圖書借閱紀錄畫面
        /// </summary>
        /// <param name="bookId"></param>
        /// <returns></returns>
        [HttpGet()]
        public ActionResult BookLendRecord(string bookId)
        {
            var id = int.Parse(bookId);
            Models.BookService bookService = new Models.BookService();
            List<Models.BookLendRecord> result = bookService.GetBookLendRecord(id);
            return View(result);
        }
    }
}