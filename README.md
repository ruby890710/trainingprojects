# Ruby

| COURSE | OUTLINE |
| ------ | ------ |
| [1-DevelopmentTool_and_Guideline](https://git.gss.com.tw/training20220214_a/ruby/-/tree/main/Course1_WorkShop) | 開發工具&套件安裝 |
| [2-SQL](https://git.gss.com.tw/training20220214_a/ruby/-/tree/main/Course2_WorkShop) | MSSQL 常用語法及進階查詢練習 |
| [3-FrontEnd_Basic](https://git.gss.com.tw/training20220214_a/ruby/-/tree/main/Course3_FrontEnd_Basic_WorkShop) | 利用 Kendo 實作一個簡單的圖書館系統 |
| [4-NET_MVC](https://git.gss.com.tw/training20220214_a/ruby/-/tree/main/Course4_NET_MVC) | 用 MVC 架構實作較完整功能的圖書館系統 |
| [5-FrontEnd_Advanced](https://git.gss.com.tw/training20220214_a/ruby/-/tree/main/Course5_FrontEnd_Advanced) | 不用 Razor 語法，用純 HTML + Kendo，前端只能透過 Ajax (非同步)與後端溝通，單頁式( SPA )->網址不會跳轉 |
| [6-BackEnd](https://git.gss.com.tw/training20220214_a/ruby/-/tree/main/Course6_Backend)-Section1 | 明確定義出 Model/Service/Dao/Common Tool，加入 Error Handle 與 Log 的處理，針對Service/Dao內的 Class 抽離出 Interface，使用 Spring .NET 實作 IOC/DI |
| [6-BackEnd](https://git.gss.com.tw/training20220214_a/ruby/-/tree/main/Course6_Backend)-Section2 | 為撰寫的程式，增加單元測試(上 Jenkins 並執行 SonarQube 掃描) |
