
var bookDataFromLocalStorage = [];
//kendo validate
var validator = $("#book_form").kendoValidator({
    messages: {
        required: "不可為空白",
        datevalidation: function (input) {
            // Return the message text.
            return input.attr("data-val-datevalidation");
        }
    },
    rules: {
        // 自訂 date validation.
        dateValidation: function (input, params) {
            if (input.is("[name='book bought date']") && input.val() != "") {
                if (new Date(input.val()) > new Date()) {
                    input.attr("data-datevalidation-msg", "您輸入未來日期，請重新輸入正確購買日期");
                    return false;
                }

                var date = kendo.parseDate(input.val(), "yyyy-MM-dd");
                input.attr("data-datevalidation-msg", "日期不存在或日期格式錯誤(yyyy-MM-dd)");
                if (date) {
                    return true;
                }
                return false;
            }
            return true;
        }
    }
});;


$(function () {
    loadBookData();
    var data = [
        //value設定為各書籍類別的圖片名稱
        { text: "資料庫", value: "database" },
        { text: "網際網路", value: "internet" },
        { text: "應用系統整合", value: "system" },
        { text: "家庭保健", value: "home" },
        { text: "語言", value: "language" }
    ]
    $("#book_category").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "value",
        dataSource: data,
        index: 0,
        change: onBookCategoryChange
    });
    $("#bought_datepicker").kendoDatePicker(
        { format: "yyyy-MM-dd" }
    );
    $("#book_grid").kendoGrid({
        dataSource: {
            data: bookDataFromLocalStorage,
            schema: {
                model: {
                    fields: {
                        BookId: { type: "int" },
                        BookName: { type: "string" },
                        BookCategory: { type: "string" },
                        BookAuthor: { type: "string" },
                        BookBoughtDate: { type: "string" }
                    }
                }
            },
            pageSize: 20,
        },
        toolbar: kendo.template("<div class='book-grid-toolbar'><input id='book_grid_search' class='book-grid-search' placeholder='我想要找......' type='text'></input></div>"),
        height: 550,
        sortable: true,
        pageable: {
            input: true,
            numeric: false
        },
        columns: [
            { field: "BookId", title: "書籍編號", width: "10%" },
            { field: "BookName", title: "書籍名稱", width: "50%" },
            { field: "BookCategory", title: "書籍種類", width: "10%" },
            { field: "BookAuthor", title: "作者", width: "15%" },
            { field: "BookBoughtDate", title: "購買日期", width: "15%" },
            { command: { text: "刪除", click: deleteBook }, title: " ", width: "120px" }
        ]
    });
})


function loadBookData() {
    bookDataFromLocalStorage = JSON.parse(localStorage.getItem("bookData"));
    if (bookDataFromLocalStorage == null) {
        bookDataFromLocalStorage = bookData;
        localStorage.setItem("bookData", JSON.stringify(bookDataFromLocalStorage));
    }
}


//新增書籍按鈕(insert_book)被點擊，開啟kendo window
function initWindow() {
    $("#kendo_window").kendoWindow({
        visible: false,
        width: "80%",
        height: "90%",
        title: "新增書籍資料",
    });
    var kendo_window = $("#kendo_window").data("kendoWindow");

    //重置kendoWindow
    $("#book_category").data("kendoDropDownList").select(0);
    $("#book_name").val("");
    $("#book_author").val("");
    $("#bought_datepicker").data("kendoDatePicker").value(new Date());
    $("#book_publisher").val("");
    onBookCategoryChange();
    $("#book_form").kendoValidator().data("kendoValidator").hideMessages();

    //定義validate
    validator = $("#book_form").kendoValidator({
        messages: {
            required: "不可為空白",
            datevalidation: function (input) {
                // Return the message text.
                return input.attr("data-val-datevalidation");
            }
        },
        rules: {
            // 自訂 date validation.
            dateValidation: function (input, params) {
                if (input.is("[name='book bought date']") && input.val() != "") {
                    if (new Date(input.val()) > new Date()) {
                        input.attr("data-datevalidation-msg", "您輸入未來日期，請重新輸入正確購買日期");
                        return false;
                    }

                    var date = kendo.parseDate(input.val(), "yyyy-MM-dd");
                    input.attr("data-datevalidation-msg", "日期不存在或日期格式錯誤(yyyy-MM-dd)");
                    if (date) {
                        return true;
                    }
                    return false;
                }
                return true;
            }
        }
    });

    //置中開啟kendo window
    kendo_window.center().open();
}


//kendoDropDownList的select值更改，一同更換書籍類別的圖片
function onBookCategoryChange() {
    var bookCategory = $("#book_category").data("kendoDropDownList").value();
    $("#book_image").attr("src", "../image/" + bookCategory + ".jpg");
}


//新增書籍並同步到localStorage
function insertBook(e) {
    var kendoGrid = $("#book_grid").data("kendoGrid");

    //---------------------新增書籍資料---------------------
    var bookCategory = $("#book_category").data("kendoDropDownList").text();
    //新增書籍的BookId = localStorage最大的BookId+1
    var bookIdInData = [];
    bookDataFromLocalStorage.forEach(item => bookIdInData.push(parseInt(item.BookId)));
    // 「...」運算子可以在函式的參數中把陣列內的數值展開
    var bookId = (Math.max(...bookIdInData) + 1).toString();
    var bookName = $("#book_name").val();
    var bookAuthor = $("#book_author").val();
    var boughtDatepicker = $("#bought_datepicker").data("kendoDatePicker").value();
    var boughtDate = kendo.toString(boughtDatepicker, "yyyy-MM-dd");
    var newObject = {
        "BookId": bookId,
        "BookCategory": bookCategory,
        "BookName": bookName,
        "BookAuthor": bookAuthor,
        "BookBoughtDate": boughtDate,
        "BookPublisher": "Ruby"
    }
    //---------------------新增書籍資料---------------------

    if (validator.data("kendoValidator").validate()) {
        bookDataFromLocalStorage.push(newObject);
        kendoGrid.dataSource.add(newObject);
        localStorage.setItem("bookData", JSON.stringify(bookDataFromLocalStorage));
        //新增成功後，關閉kendo window
        $("#kendo_window").data("kendoWindow").close();

        //顯示新增成功提示
        $('#msg').css("color", "green");
        $('#msg').html("已新增：(" + bookId + ") " + bookName).fadeIn('slow');
        $('#msg').delay(1000).fadeOut('slow');
    }

}


// 搜尋書籍功能
function searchBook() {
    $("#book_grid_search").on("input", function (e) {
        var value = $(this).val().toLowerCase();
        var kendoGridDataSource = $("#book_grid").data("kendoGrid").dataSource;
        // 只要欄位中有該字詞則顯示
        kendoGridDataSource.filter({
            logic: "or",
            filters: [
                { field: "BookId", operator: "contains", value: value },
                { field: "BookName", operator: "contains", value: value },
                { field: "BookCategory", operator: "contains", value: value },
                { field: "BookAuthor", operator: "contains", value: value },
                { field: "BookBoughtDate", operator: "contains", value: value }
            ]
        });
        // 只能顯示特定頁搜索結果
        // $("#book_grid tr").filter(function () {
        //     //隱藏與搜索不匹配的行
        //     $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        // });
    });
}


//刪除書籍並同步到localStorage
function deleteBook(event) {
    //取消目標元素的預設行為
    event.preventDefault();
    //取得使用者選取的刪除列
    var getItem = this.dataItem($(event.target).closest("tr"));
    var dataSource = $("#book_grid").data("kendoGrid").dataSource;
    //刪除confirm
    if (confirm("是否要刪除：(" + getItem.get("BookId") + ") " + getItem.get("BookName") + "?")) {
        dataSource.remove(getItem);
        for (var i = 0; i < bookDataFromLocalStorage.length; i++) {
            if (bookDataFromLocalStorage[i]["BookId"] === getItem.get("BookId")) {
                bookDataFromLocalStorage.splice(i, 1);
                break;
            }
        }
        localStorage.setItem("bookData", JSON.stringify(bookDataFromLocalStorage));
        //顯示刪除成功提示
        $('#msg').css("color", "red");
        $('#msg').html("已刪除：(" + getItem.get("BookId") + ") " + getItem.get("BookName")).fadeIn('slow');
        $('#msg').delay(1000).fadeOut('slow');
    }
}